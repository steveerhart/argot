#include <QGuiApplication>
#include <QtCore>
#include <QtGui>
#include <QtCore/QUrl>
#include <QtQuick>

#include <QQmlComponent>
#include <QQmlEngine>

int main(int argc, char* argv[]) {

  QGuiApplication app(argc, argv);

  QUrl qmlSource("qrc:/qml/main");

  QQmlEngine *engine = new QQmlEngine();
  app.connect(engine, SIGNAL(quit()), SLOT(quit()));
  
  
  QQmlComponent *component = new QQmlComponent(engine, qmlSource);
  component->loadUrl(qmlSource);
  
  if(!component->isReady()) {
    qWarning("%s", qPrintable(component->errorString()));
    return -1;
  }
  

  QObject *window = component->create();
  QQuickWindow *mainWindow = qobject_cast<QQuickWindow*>(window);
  
  mainWindow->show();

  return app.exec();

}
