set(CMAKE_INCLUDE_CURRENT_DIR ON)

file(GLOB_RECURSE CODE_FILES *.cpp)

qt5_add_resources(RESOURCE_FILES ../resources/resources.qrc)

add_executable(${CMAKE_PROJECT_NAME} ${CODE_FILES} ${RESOURCE_FILES})
qt5_use_modules(${CMAKE_PROJECT_NAME} Widgets Network Qml Quick Declarative Core Gui)

if (QXMPP_FOUND)
  include_directories(${QXMPP_INCLUDE_DIRS})
  target_link_libraries (${CMAKE_PROJECT_NAME} ${QXMPP_LIBRARIES})
endif (QXMPP_FOUND)
